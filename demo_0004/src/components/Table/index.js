import React, { Component } from 'react';

import './styles.css';

class Table extends Component {
    render() {
        return TableSVG;
    }
}

export default Table;

const TableSVG = <div className="table-container">
    <svg version="1.1" id="Layer_1" className="table-svg" viewBox="0 0 400 165">

        <title>American roulette layout</title>
        <rect fill="#060" width="100%" height="100%" />
        <g transform="matrix(1 0 0 1 -150 0)">
            <g>
                <g id="XMLID_4_">
                    <g>
                        <rect x="488.647" y="71.917" fill="#f00" width="26.914" height="33.35" />
                        <rect x="488.647" y="38.614" width="26.914" height="33.303" />
                        <rect x="488.647" y="5.264" fill="#f00" width="26.914" height="33.35" />
                        <rect x="461.731" y="71.917" width="26.916" height="33.35" />
                        <rect x="461.731" y="38.614" fill="#f00" width="26.916" height="33.303" />
                        <rect x="461.731" y="5.264" width="26.916" height="33.35" />
                        <rect x="434.817" y="71.917" fill="#f00" width="26.914" height="33.35" />
                        <rect x="434.817" y="38.614" width="26.914" height="33.303" />
                        <rect x="434.817" y="5.264" fill="#f00" width="26.914" height="33.35" />
                        <rect x="407.901" y="71.917" fill="#f00" width="26.916" height="33.35" />
                        <rect x="407.901" y="38.614" width="26.916" height="33.303" />
                        <rect x="407.901" y="5.264" fill="#f00" width="26.916" height="33.35" />
                        <rect x="380.977" y="71.917" width="26.924" height="33.35" />
                        <rect x="380.977" y="38.614" fill="#f00" width="26.924" height="33.303" />
                        <rect x="380.977" y="5.264" width="26.924" height="33.35" />
                        <rect x="353.977" y="71.917" width="27" height="33.35" />
                        <rect x="353.977" y="38.614" width="27" height="33.303" />
                        <rect x="353.977" y="5.264" fill="#f00" width="27" height="33.35" />
                        <rect x="327.063" y="71.917" fill="#f00" width="26.914" height="33.35" />
                        <rect x="327.063" y="38.614" width="26.914" height="33.303" />
                        <rect x="327.063" y="5.264" fill="#f00" width="26.914" height="33.35" />
                        <rect x="300.147" y="71.917" width="26.916" height="33.35" />
                        <rect x="300.147" y="38.614" fill="#f00" width="26.916" height="33.303" />
                        <rect x="300.147" y="5.264" width="26.916" height="33.35" />
                        <rect x="273.233" y="71.917" width="26.914" height="33.35" />
                        <rect x="273.233" y="38.614" width="26.914" height="33.303" />
                        <rect x="273.233" y="5.264" fill="#f00" width="26.914" height="33.35" />
                        <rect x="246.317" y="71.917" fill="#f00" width="26.916" height="33.35" />
                        <rect x="246.317" y="38.614" width="26.916" height="33.303" />
                        <rect x="246.317" y="5.264" fill="#f00" width="26.916" height="33.35" />
                        <rect x="219.401" y="71.917" width="26.916" height="33.35" />
                        <rect x="219.401" y="38.614" fill="#f00" width="26.916" height="33.303" />
                        <rect x="219.401" y="5.264" width="26.916" height="33.35" />
                        <rect x="192.477" y="71.917" fill="#f00" width="26.925" height="33.35" />
                        <rect x="192.477" y="38.614" width="26.925" height="33.303" />
                        <rect x="192.477" y="5.264" fill="#f00" width="26.925" height="33.35" />
                    </g>
                    <g>
                        <polygon fill="none" stroke="#fff" points="192.477,38.614 192.477,5.264 219.401,5.264 246.317,5.264 273.233,5.264       300.147,5.264 327.063,5.264 353.977,5.264 380.977,5.264 407.901,5.264 434.817,5.264 461.731,5.264 488.647,5.264       515.561,5.264 542.477,5.264 542.477,38.614 542.477,71.917 542.477,105.266 515.561,105.266 488.647,105.266 461.731,105.266       434.817,105.266 407.901,105.266 380.977,105.266 353.977,105.266 327.063,105.266 300.147,105.266 273.233,105.266       246.317,105.266 219.401,105.266 192.477,105.266 192.477,71.917     " />
                        <polyline fill="none" stroke="#fff" points="192.477,71.917 219.401,71.917 246.317,71.917 273.233,71.917 300.147,71.917       327.063,71.917 353.977,71.917 380.977,71.917 407.901,71.917 434.817,71.917 461.731,71.917 488.647,71.917 515.561,71.917       542.477,71.917     " />
                        <polyline fill="none" stroke="#fff" points="192.477,38.614 219.401,38.614 246.317,38.614 273.233,38.614 300.147,38.614       327.063,38.614 353.977,38.614 380.977,38.614 407.901,38.614 434.817,38.614 461.731,38.614 488.647,38.614 515.561,38.614       542.477,38.614     " />
                        <polyline fill="none" stroke="#fff" points="515.561,5.264 515.561,38.614 515.561,71.917 515.561,105.266     " />
                        <polyline fill="none" stroke="#fff" points="488.647,5.264 488.647,38.614 488.647,71.917 488.647,105.266     " />
                        <polyline fill="none" stroke="#fff" points="461.731,5.264 461.731,38.614 461.731,71.917 461.731,105.266     " />
                        <polyline fill="none" stroke="#fff" points="434.817,5.264 434.817,38.614 434.817,71.917 434.817,105.266     " />
                        <polyline fill="none" stroke="#fff" points="407.901,5.264 407.901,38.614 407.901,71.917 407.901,105.266     " />
                        <polyline fill="none" stroke="#fff" points="380.977,5.264 380.977,38.614 380.977,71.917 380.977,105.266     " />
                        <polyline fill="none" stroke="#fff" points="353.977,5.264 353.977,38.614 353.977,71.917 353.977,105.266     " />
                        <polyline fill="none" stroke="#fff" points="327.063,5.264 327.063,38.614 327.063,71.917 327.063,105.266     " />
                        <polyline fill="none" stroke="#fff" points="300.147,5.264 300.147,38.614 300.147,71.917 300.147,105.266     " />
                        <polyline fill="none" stroke="#fff" points="273.233,5.264 273.233,38.614 273.233,71.917 273.233,105.266     " />
                        <polyline fill="none" stroke="#fff" points="246.317,5.264 246.317,38.614 246.317,71.917 246.317,105.266     " />
                        <polyline fill="none" stroke="#fff" points="219.401,5.264 219.401,38.614 219.401,71.917 219.401,105.266     " />
                    </g>
                </g>
                <g>
                    <path fill="none" stroke="#fff" d="M192.083,55.265c-6.813,0-13.626,0-20.439,0l-6.561-25.5l6.561-24.5     c6.813,0,13.626,0,20.439,0" />
                    <path fill="none" stroke="#fff" d="M192.083,105.266c-6.813,0-13.626,0-20.439,0l-6.561-25.501l6.561-24.5     c6.813,0,13.626,0,20.439,0" />
                </g>
                <g>
                    <g>
                        <g id="XMLID_5_">
                            <g>
                                <rect x="353.647" y="125.563" width="53.859" height="20" />
                                <rect x="299.788" y="125.563" fill="#f00" width="53.859" height="20" />
                            </g>
                            <g>
                                <polygon fill="none" stroke="#fff" points="192.146,125.563 192.146,105.563 299.788,105.563 407.506,105.563         515.147,105.563 515.147,125.563 515.147,145.563 461.288,145.563 407.506,145.563 353.647,145.563 299.788,145.563         246.006,145.563 192.146,145.563       " />
                                <polyline fill="none" stroke="#fff" points="192.146,125.563 246.006,125.563 299.788,125.563 353.647,125.563         407.506,125.563 461.288,125.563 515.147,125.563       " />
                                <line fill="none" stroke="#fff" x1="461.288" y1="125.563" x2="461.288" y2="145.563" />
                                <polyline fill="none" stroke="#fff" points="407.506,105.563 407.506,125.563 407.506,145.563       " />
                                <line fill="none" stroke="#fff" x1="353.647" y1="125.563" x2="353.647" y2="145.563" />
                                <polyline fill="none" stroke="#fff" points="299.788,105.563 299.788,125.563 299.788,145.563       " />
                                <line fill="none" stroke="#fff" x1="246.006" y1="125.563" x2="246.006" y2="145.563" />
                            </g>
                        </g>
                    </g>
                </g>
            </g>
            <g>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 185.2476 85.5403)" fill="#fff" font-family="'Verdana'" font-size="14">0</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 185.2476 40.0403)" fill="#fff" font-family="'Verdana'" font-size="14">00</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 210.5728 93.2278)" fill="#fff" font-family="'Verdana'" font-size="14">1</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 210.5728 60.1829)" fill="#fff" font-family="'Verdana'" font-size="14">2</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 210.5728 26.5012)" fill="#fff" font-family="'Verdana'" font-size="14">3</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 238.2407 93.2278)" fill="#fff" font-family="'Verdana'" font-size="14">4</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 238.2407 26.5012)" fill="#fff" font-family="'Verdana'" font-size="14">6</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 238.2407 60.1829)" fill="#fff" font-family="'Verdana'" font-size="14">5</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 264.6743 93.2278)" fill="#fff" font-family="'Verdana'" font-size="14">7</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 264.6743 60.1829)" fill="#fff" font-family="'Verdana'" font-size="14">8</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 264.6743 26.5012)" fill="#fff" font-family="'Verdana'" font-size="14">9</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 291.3423 97.679)" fill="#fff" font-family="'Verdana'" font-size="14">10</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 291.3423 30.9524)" fill="#fff" font-family="'Verdana'" font-size="14">12</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 291.3423 64.634)" fill="#fff" font-family="'Verdana'" font-size="14">11</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 318.9966 97.679)" fill="#fff" font-family="'Verdana'" font-size="14">13</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 318.9966 64.634)" fill="#fff" font-family="'Verdana'" font-size="14">14</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 318.9966 30.9524)" fill="#fff" font-family="'Verdana'" font-size="14">15</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 345.6655 97.679)" fill="#fff" font-family="'Verdana'" font-size="14">16</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 345.6655 30.9524)" fill="#fff" font-family="'Verdana'" font-size="14">18</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 345.6655 64.634)" fill="#fff" font-family="'Verdana'" font-size="14">17</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 372.6538 97.679)" fill="#fff" font-family="'Verdana'" font-size="14">19</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 372.6538 64.634)" fill="#fff" font-family="'Verdana'" font-size="14">20</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 372.6538 30.9524)" fill="#fff" font-family="'Verdana'" font-size="14">21</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 399.3228 97.679)" fill="#fff" font-family="'Verdana'" font-size="14">22</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 399.3228 30.9524)" fill="#fff" font-family="'Verdana'" font-size="14">24</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 399.3228 64.634)" fill="#fff" font-family="'Verdana'" font-size="14">23</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 426.7544 97.679)" fill="#fff" font-family="'Verdana'" font-size="14">25</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 426.7554 64.634)" fill="#fff" font-family="'Verdana'" font-size="14">26</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 426.7544 30.9524)" fill="#fff" font-family="'Verdana'" font-size="14">27</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 453.4233 97.679)" fill="#fff" font-family="'Verdana'" font-size="14">28</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 453.4233 30.9524)" fill="#fff" font-family="'Verdana'" font-size="14">30</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 453.4243 64.634)" fill="#fff" font-family="'Verdana'" font-size="14">29</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 479.9644 97.679)" fill="#fff" font-family="'Verdana'" font-size="14">31</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 479.9653 64.634)" fill="#fff" font-family="'Verdana'" font-size="14">32</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 479.9644 30.9524)" fill="#fff" font-family="'Verdana'" font-size="14">33</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 506.6333 97.679)" fill="#fff" font-family="'Verdana'" font-size="14">34</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 506.6333 30.9524)" fill="#fff" font-family="'Verdana'" font-size="14">36</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 506.6343 64.634)" fill="#fff" font-family="'Verdana'" font-size="14">35</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 533.7954 34.1301)" fill="#fff" font-family="'Verdana'" font-size="14">2-1</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 533.7954 100.8567)" fill="#fff" font-family="'Verdana'" font-size="14">2-1</text>

                <text transform="matrix(-4.371139e-008 -1 1 -4.371139e-008 533.7954 67.8118)" fill="#fff" font-family="'Verdana'" font-size="14">2-1</text>
                <text transform="matrix(1 0 0 1 227.0972 120.0349)" fill="#fff" font-family="'Verdana'" font-size="12">1st 12</text>
                <text transform="matrix(1 0 0 1 332.7505 120.0349)" fill="#fff" font-family="'Verdana'" font-size="12">2nd 12</text>
                <text transform="matrix(1 0 0 1 441.811 120.0339)" fill="#fff" font-family="'Verdana'" font-size="12">3rd 12</text>
                <text transform="matrix(1 0 0 1 205.8198 139.8386)" fill="#fff" font-family="'Verdana'" font-size="12">1-18</text>
                <text transform="matrix(1 0 0 1 258.7339 139.8386)" fill="#fff" font-family="'Verdana'" font-size="12">Even</text>
                <text transform="matrix(1 0 0 1 422.6274 139.8376)" fill="#fff" font-family="'Verdana'" font-size="12">Odd</text>
                <text transform="matrix(1 0 0 1 470.4556 139.8386)" fill="#fff" font-family="'Verdana'" font-size="12">19-36</text>
                <text transform="matrix(1 0 0 1 316.4204 139.8376)" fill="#fff" font-family="'Verdana'" font-size="12">Red</text>
                <text transform="matrix(1 0 0 1 365.979 139.8376)" fill="#fff" font-family="'Verdana'" font-size="12">Black</text>
            </g>
        </g>
        {/* <g>
	<g>
		<g id="XMLID_12_">
			<g>
				<path fill="#f00" d="M8.542,107.556c-1.361-4.01-2.398-8.157-3.075-12.417l0.027-0.012l76.435-12.371l-0.014,0.013      L8.542,107.556z"/>
				<rect x="81.941" y="82.76" width="0.013" height="0.014"/>
				<path fill="#f00" d="M143.351,35.403L82.009,82.725l-0.013-0.001l52.727-56.736l0.014-0.013      C137.868,28.868,140.742,32.023,143.351,35.403z"/>
				<path d="M5.6,69.611c0.735-4.254,1.795-8.374,3.179-12.338l0.025,0.016l73.127,25.442l0.013,0.003l-0.001,0.013l-0.052-0.005      L5.6,69.611z"/>
				<path fill="#f00" d="M4.78,76.008c0.189-2.16,0.467-4.302,0.819-6.396l76.291,13.13L4.477,82.426      C4.496,80.301,4.592,78.157,4.78,76.008z"/>
				<path d="M50.512,153.612c-3.944-1.745-7.692-3.813-11.233-6.155l42.673-64.671L50.512,153.612z"/>
				<path d="M113.421,11.91L81.984,82.711l19.357-74.995C105.527,8.783,109.565,10.199,113.421,11.91z"/>
				<path fill="#f00" d="M8.779,57.273c1.406-4.064,3.149-7.982,5.185-11.694l67.968,37.14l-0.001,0.014L8.804,57.289      L8.779,57.273z"/>
				<rect x="81.983" y="82.723" fill="#f00" width="0.013" height="0.014"/>
				<rect x="81.941" y="82.747" fill="#f00" width="0.013" height="0.013"/>
				<path fill="#f00" d="M149.997,119.932c-2.056,3.786-4.417,7.354-7.056,10.7L82.016,82.791l67.967,37.142H149.997z"/>
				<polygon fill="#f00" points="81.941,82.759 81.94,82.771 81.914,82.769 81.928,82.758     "/>
				<path fill="#f00" d="M50.512,153.612l31.439-70.826l-19.344,74.996l-0.002,0.024      C58.419,156.742,54.369,155.324,50.512,153.612z"/>
				<rect x="81.968" y="82.736" fill="#f00" width="0.013" height="0.013"/>
				<path d="M158.362,95.888l-0.013-0.003L82.045,82.755l77.425,0.317l0.013,0.001c-0.018,2.125-0.115,4.28-0.305,6.442      C158.991,91.665,158.714,93.793,158.362,95.888z"/>
				<path d="M124.088,147.836c-3.588,2.331-7.37,4.359-11.304,6.063L81.989,82.802l0.001-0.013l42.086,65.032l-0.001,0.013      L124.088,147.836z"/>
				<path d="M4.478,82.425l77.413,0.316l0.052,0.005l-0.001,0.013l-0.013-0.001L5.493,95.129L5.466,95.14      C4.792,90.996,4.463,86.742,4.478,82.425z"/>
				<path fill="#f00" d="M101.342,7.716L81.985,82.711l-0.014,0.013l6.75-77.161l0.013,0.001      C93.057,5.94,97.272,6.673,101.342,7.716z"/>
				<path fill="#f00" d="M150.315,46.189l0.014-0.013c2.005,3.755,3.723,7.69,5.09,11.765l-0.027,0.012L82.008,82.725      L150.315,46.189z"/>
				<path fill="#f00" d="M20.598,130.094c-2.609-3.379-4.938-6.979-6.966-10.772l68.282-36.552l0.026,0.002L20.598,130.094z"/>
				<path d="M155.42,57.941c1.359,4.021,2.397,8.171,3.074,12.431l-0.04,0.01L82.007,82.739l0.001-0.014l73.384-24.772      L155.42,57.941z"/>
				<polygon points="81.995,82.738 81.981,82.749 81.982,82.737     "/>
				<rect x="81.943" y="82.733" width="0.013" height="0.014"/>
				<path d="M13.632,119.321c-2.018-3.756-3.724-7.678-5.09-11.765l73.373-24.787L13.632,119.321z"/>
				<path fill="#f00" d="M100.653,157.998L81.987,82.813l0.001-0.014l30.795,71.099      C108.89,155.581,104.841,156.965,100.653,157.998z"/>
				<path d="M142.941,130.634c-2.656,3.386-5.576,6.526-8.744,9.399L82.016,82.791L142.941,130.634z"/>
				<path fill="#f00" d="M21.02,34.878c2.655-3.372,5.575-6.513,8.731-9.399l52.181,57.241L21.02,34.878z"/>
				<path d="M63.306,7.526l18.652,75.182L51.174,11.638l0.001-0.014c3.894-1.683,7.943-3.064,12.118-4.101L63.306,7.526z"/>
				<path d="M81.98,82.76l-0.001,0.016l-6.753,77.186c-4.336-0.379-8.55-1.109-12.621-2.153l0.002-0.024l19.345-74.996l0.015-0.023      l0.001-0.014l0.013,0.001L81.98,82.76z"/>
				<rect x="81.979" y="82.762" width="0.013" height="0.015"/>
				<polygon fill="#f00" points="81.97,82.722 81.969,82.736 81.956,82.735 81.958,82.721 81.959,82.708     "/>
				<path d="M29.751,25.476c3.155-2.875,6.547-5.481,10.121-7.802l0.012,0.015l42.074,65.032l-0.001,0.014l-0.013-0.002      l-0.012-0.016L29.751,25.476z"/>
				<path d="M20.598,130.094L81.94,82.771l-52.715,56.752l-0.014,0.013C26.093,136.643,23.207,133.473,20.598,130.094z"/>
				<path fill="#f00" d="M134.196,140.046c-3.156,2.875-6.535,5.483-10.107,7.79l-0.013-0.002l0.001-0.013L81.991,82.789      l0.001-0.013l0.025,0.015l52.181,57.242L134.196,140.046z"/>
				<rect x="81.955" y="82.748" width="0.013" height="0.013"/>
				<polygon points="81.932,82.719 81.943,82.733 81.931,82.731     "/>
				<polygon fill="#f00" points="81.967,82.76 81.952,82.785 81.953,82.772 81.954,82.76     "/>
				<rect x="81.995" y="82.724" fill="#f00" width="0.013" height="0.014"/>
				<path fill="#f00" d="M124.656,18.052L81.984,82.712l31.437-70.801l0.013,0.001C117.366,13.656,121.114,15.708,124.656,18.052      z"/>
				<path fill="#f00" d="M75.929,5.506l6.029,77.202L63.306,7.525C67.401,6.51,71.624,5.815,75.929,5.506z"/>
				<path d="M124.656,18.052c3.592,2.363,6.952,5.004,10.081,7.923l-0.014,0.013L81.996,82.724l-0.013-0.001l0.001-0.014      L124.656,18.052z"/>
				<path fill="#f00" d="M39.279,147.456c-3.58-2.362-6.938-5.017-10.068-7.921l0.014-0.013L81.94,82.771l0.013,0.001      l-0.001,0.014L39.279,147.456z"/>
				<path fill="#f00" d="M39.872,17.676c3.586-2.317,7.368-4.347,11.302-6.037l30.784,71.069l-0.001,0.015L39.883,17.689      L39.872,17.676z"/>
				<polygon points="81.984,82.711 81.983,82.723 81.982,82.737 81.969,82.736 81.97,82.722     "/>
				<path fill="#f00" d="M158.362,95.888c-0.735,4.253-1.797,8.388-3.181,12.351l-0.025-0.003L81.993,82.763L81.98,82.76      l0.001-0.013l0.064,0.006l76.304,13.132L158.362,95.888z"/>
				<path d="M13.964,45.579c2.055-3.773,4.417-7.354,7.056-10.701l60.912,47.841L13.964,45.579z"/>
				<path d="M88.016,160.031l0.001-0.014l-6.029-77.203l18.666,75.185C96.558,159.014,92.323,159.707,88.016,160.031z"/>
				<path d="M82.009,82.725l61.342-47.322c2.607,3.392,4.95,6.993,6.979,10.773l-0.014,0.013L82.009,82.725z"/>
				<polygon points="81.991,82.777 81.99,82.789 81.989,82.802 81.979,82.775     "/>
				<path fill="#f00" d="M159.483,83.074l-0.013-0.001l-77.425-0.317l-0.064-0.006l0.014-0.012l0.013,0.001l76.447-12.357      l0.04-0.01C159.169,74.514,159.498,78.757,159.483,83.074z"/>
				<rect x="81.956" y="82.735" fill="#f00" width="0.013" height="0.013"/>
				<path d="M155.181,108.239c-1.407,4.076-3.148,7.982-5.185,11.693l-0.013-0.001L82.016,82.791l-0.025-0.016l0.001-0.013      l73.163,25.473L155.181,108.239z"/>
			</g>
			<g>
				<path fill="none" stroke="#000" d="M134.196,140.046c-3.156,2.875-6.535,5.483-10.107,7.79      c-3.588,2.331-7.37,4.359-11.304,6.063c-3.894,1.683-7.943,3.064-12.131,4.1c-4.095,1.018-8.331,1.709-12.637,2.032      c-4.19,0.319-8.467,0.31-12.791-0.069c-4.336-0.379-8.55-1.109-12.621-2.153c-4.185-1.064-8.236-2.482-12.092-4.194      c-3.944-1.745-7.692-3.813-11.233-6.155c-3.58-2.361-6.938-5.016-10.068-7.921c-3.118-2.893-6.004-6.063-8.612-9.439      c-2.609-3.38-4.938-6.979-6.966-10.773c-2.018-3.755-3.724-7.678-5.09-11.765c-1.361-4.01-2.398-8.156-3.075-12.417      c-0.675-4.145-1.003-8.397-0.988-12.716c0.018-2.123,0.114-4.269,0.302-6.417c0.189-2.16,0.467-4.302,0.819-6.396      c0.735-4.253,1.795-8.374,3.179-12.337c1.406-4.065,3.149-7.983,5.185-11.695c2.055-3.773,4.417-7.354,7.056-10.701      c2.655-3.372,5.575-6.513,8.731-9.399c3.155-2.875,6.547-5.481,10.121-7.801c3.586-2.317,7.368-4.348,11.302-6.038"/>
				<path fill="none" stroke="#000" d="M113.434,11.912c3.932,1.744,7.68,3.796,11.222,6.143      c3.592,2.362,6.952,5.003,10.081,7.922c3.131,2.894,6.005,6.048,8.614,9.428c2.607,3.392,4.95,6.993,6.979,10.773      c2.005,3.755,3.723,7.69,5.09,11.765c1.359,4.021,2.397,8.171,3.074,12.431c0.675,4.144,1.004,8.385,0.989,12.702      c-0.018,2.125-0.115,4.28-0.305,6.442c-0.188,2.147-0.465,4.275-0.816,6.372c-0.735,4.253-1.797,8.388-3.181,12.351      c-1.407,4.076-3.148,7.982-5.185,11.693c-2.056,3.786-4.417,7.354-7.056,10.7c-2.656,3.386-5.576,6.526-8.744,9.399"/>
				<path fill="none" stroke="#000" d="M75.93,5.493c4.19-0.32,8.467-0.31,12.791,0.068l0.013,0.001      c4.323,0.378,8.538,1.109,12.608,2.152c4.185,1.066,8.223,2.483,12.08,4.194"/>
				<path fill="none" stroke="#000" d="M63.306,7.526c4.095-1.018,8.317-1.711,12.623-2.021"/>
				<path fill="none" stroke="#000" d="M51.175,11.625c3.894-1.682,7.943-3.064,12.118-4.101"/>
				<line fill="none" stroke="#000" x1="81.979" y1="82.775" x2="75.226" y2="159.961"/>
				<line fill="none" stroke="#000" x1="81.988" y1="82.814" x2="88.017" y2="160.017"/>
				<line fill="none" stroke="#000" x1="81.979" y1="82.775" x2="81.989" y2="82.802"/>
				<polyline fill="none" stroke="#000" points="81.989,82.802 81.988,82.814 100.653,157.998 100.652,158.01     "/>
				<polyline fill="none" stroke="#000" points="81.99,82.789 81.989,82.802 112.784,153.9     "/>
				<polyline fill="none" stroke="#000" points="81.98,82.76 81.979,82.775 81.991,82.777 81.99,82.789 124.077,147.822     "/>
				<polyline fill="none" stroke="#000" points="82.016,82.791 134.197,140.034 134.196,140.046 134.209,140.047     "/>
				<line fill="none" stroke="#000" x1="82.016" y1="82.791" x2="142.941" y2="130.634"/>
				<polyline fill="none" stroke="#000" points="81.993,82.763 81.991,82.777 82.016,82.791 149.983,119.931     "/>
				<polyline fill="none" stroke="#000" points="81.98,82.76 81.993,82.763 155.156,108.236     "/>
				<line fill="none" stroke="#000" x1="82.045" y1="82.755" x2="158.349" y2="95.885"/>
				<polyline fill="none" stroke="#000" points="81.981,82.749 82.045,82.755 159.47,83.073     "/>
				<line fill="none" stroke="#000" x1="82.008" y1="82.739" x2="158.455" y2="70.382"/>
				<line fill="none" stroke="#000" x1="81.981" y1="82.749" x2="81.995" y2="82.738"/>
				<polyline fill="none" stroke="#000" points="81.995,82.738 82.008,82.739 82.009,82.725 155.393,57.952     "/>
				<line fill="none" stroke="#000" x1="82.009" y1="82.725" x2="150.315" y2="46.189"/>
				<polyline fill="none" stroke="#000" points="81.968,82.748 81.981,82.749 81.982,82.737 81.995,82.738 81.996,82.724       82.009,82.725 143.351,35.403     "/>
				<polyline fill="none" stroke="#000" points="81.983,82.723 81.996,82.724 134.723,25.988     "/>
				<polyline fill="none" stroke="#000" points="81.984,82.711 124.656,18.052 124.67,18.042     "/>
				<polyline fill="none" stroke="#000" points="81.969,82.736 81.982,82.737 81.983,82.723 81.984,82.711 113.421,11.91       113.434,11.912 113.435,11.898     "/>
				<polyline fill="none" stroke="#000" points="81.97,82.722 81.984,82.711 101.342,7.716     "/>
				<polyline fill="none" stroke="#000" points="81.97,82.722 88.721,5.561 88.722,5.549     "/>
				<polyline fill="none" stroke="#000" points="81.969,82.736 81.97,82.722 81.959,82.708 75.929,5.506 75.93,5.493     "/>
				<polyline fill="none" stroke="#000" points="81.959,82.708 63.306,7.526 63.293,7.524 63.294,7.51     "/>
				<polyline fill="none" stroke="#000" points="81.958,82.721 81.959,82.708 51.174,11.637 51.175,11.625 51.164,11.611     "/>
				<polyline fill="none" stroke="#000" points="81.968,82.748 81.969,82.736 81.956,82.735 81.958,82.721 39.884,17.689     "/>
				<polyline fill="none" stroke="#000" points="81.932,82.719 29.751,25.476 29.739,25.463     "/>
				<polyline fill="none" stroke="#000" points="81.943,82.733 81.932,82.719 21.02,34.878     "/>
				<polyline fill="none" stroke="#000" points="81.931,82.731 81.932,82.719 13.964,45.579     "/>
				<polyline fill="none" stroke="#000" points="81.955,82.747 81.956,82.735 81.943,82.733     "/>
				<polyline fill="none" stroke="#000" points="81.942,82.746 81.943,82.733 81.931,82.731 8.804,57.289     "/>
				<line fill="none" stroke="#000" x1="81.891" y1="82.742" x2="5.6" y2="69.611"/>
				<polyline fill="none" stroke="#000" points="81.942,82.746 81.891,82.742 4.478,82.425     "/>
				<line fill="none" stroke="#000" x1="81.928" y1="82.758" x2="5.493" y2="95.129"/>
				<polyline fill="none" stroke="#000" points="81.955,82.747 81.942,82.746 81.941,82.759     "/>
				<polyline fill="none" stroke="#000" points="81.941,82.759 81.928,82.758 81.914,82.769 8.542,107.556     "/>
				<polyline fill="none" stroke="#000" points="81.94,82.771 81.914,82.769 13.632,119.321     "/>
				<polyline fill="none" stroke="#000" points="81.968,82.748 81.955,82.747 81.954,82.76 81.941,82.759 81.94,82.771       20.598,130.094 20.584,130.106     "/>
				<polyline fill="none" stroke="#000" points="81.953,82.772 81.94,82.771 29.225,139.523     "/>
				<polyline fill="none" stroke="#000" points="81.952,82.785 39.279,147.456 39.277,147.469     "/>
				<polyline fill="none" stroke="#000" points="81.967,82.76 81.954,82.76 81.953,82.772 81.952,82.785 50.512,153.612     "/>
				<polyline fill="none" stroke="#000" points="81.968,82.748 81.967,82.76 81.952,82.785 62.607,157.782     "/>
			</g>
		</g>
	</g>
</g>
<path fill="#fff" stroke="#000" d="M146.313,88.389c3.111-35.563-23.142-66.85-58.705-69.96  c-35.562-3.109-66.85,23.143-69.961,58.704c-3.112,35.563,23.142,66.851,58.704,69.961  C111.913,150.206,143.201,123.953,146.313,88.389z"/>
<text transform="matrix(1 0 0 1 79.7827 14.6707)" fill="#fff" font-family="'Verdana'" font-size="7">0</text>
<text transform="matrix(0.9877 0.1564 -0.1564 0.9877 88.9224 14.9719)" fill="#fff" font-family="'Verdana'" font-size="7">28</text>
<text transform="matrix(0.8829 0.4695 -0.4695 0.8829 111.1484 20.4949)" fill="#fff" font-family="'Verdana'" font-size="7">26</text>
<text transform="matrix(0.788 0.6157 -0.6157 0.788 120.8687 26.116)" fill="#fff" font-family="'Verdana'" font-size="7">30</text>
<text transform="matrix(0.682 0.7314 -0.7314 0.682 129.8857 33.2498)" fill="#fff" font-family="'Verdana'" font-size="7">11</text>
<text transform="matrix(0.5446 0.8387 -0.8387 0.5446 138.8442 43.95)" fill="#fff" font-family="'Verdana'" font-size="7">7</text>
<text transform="matrix(0.4067 0.9135 -0.9135 0.4067 143.4814 51.5706)" fill="#fff" font-family="'Verdana'" font-size="7">20</text>
<text transform="matrix(0.2419 0.9703 -0.9703 0.2419 146.7808 61.9568)" fill="#fff" font-family="'Verdana'" font-size="7">32</text>
<text transform="matrix(0.0872 0.9962 -0.9962 0.0872 149.6606 72.7351)" fill="#fff" font-family="'Verdana'" font-size="7">17</text>
<text transform="matrix(-0.0872 0.9962 -0.9962 -0.0872 150.9053 86.7576)" fill="#fff" font-family="'Verdana'" font-size="7">5</text>
<text transform="matrix(-0.2419 0.9703 -0.9703 -0.2419 149.2026 95.5872)" fill="#fff" font-family="'Verdana'" font-size="7">22</text>
<text transform="matrix(-0.4067 0.9135 -0.9135 -0.4067 146.5288 106.6023)" fill="#fff" font-family="'Verdana'" font-size="7">34</text>
<text transform="matrix(-0.5446 0.8387 -0.8387 -0.5446 141.6787 117.0129)" fill="#fff" font-family="'Verdana'" font-size="7">15</text>
<text transform="matrix(-0.682 0.7314 -0.7314 -0.682 133.8442 128.4651)" fill="#fff" font-family="'Verdana'" font-size="7">3</text>
<text transform="matrix(-0.788 0.6157 -0.6157 -0.788 127.1035 134.8923)" fill="#fff" font-family="'Verdana'" font-size="7">24</text>
<text transform="matrix(0.9455 0.3256 -0.3256 0.9455 102.3945 17.2312)" fill="#fff" font-family="'Verdana'" font-size="7">9</text>
<text transform="matrix(-0.8829 0.4695 -0.4695 -0.8829 118.3472 141.7668)" fill="#fff" font-family="'Verdana'" font-size="7">36</text>
<text transform="matrix(-0.9455 0.3256 -0.3256 -0.9455 108.0762 146.0208)" fill="#fff" font-family="'Verdana'" font-size="7">13</text>
<text transform="matrix(-0.9877 0.1564 -0.1564 -0.9877 94.729 150.5149)" fill="#fff" font-family="'Verdana'" font-size="7">1</text>
<text transform="matrix(-1 0 0 -1 86.0186 151.6399)" fill="#fff" font-family="'Verdana'" font-size="7">00</text>
<text transform="matrix(-0.9877 -0.1564 0.1564 -0.9877 74.0854 151.363)" fill="#fff" font-family="'Verdana'" font-size="7">27</text>
<text transform="matrix(-0.9455 -0.3256 0.3256 -0.9455 62.7144 149.9929)" fill="#fff" font-family="'Verdana'" font-size="7">10</text>
<text transform="matrix(-0.8829 -0.4695 0.4695 -0.8829 52.4136 145.2878)" fill="#fff" font-family="'Verdana'" font-size="7">25</text>
<text transform="matrix(-0.788 -0.6157 0.6157 -0.788 42.9321 140.031)" fill="#fff" font-family="'Verdana'" font-size="7">29</text>
<text transform="matrix(-0.682 -0.7314 0.7314 -0.682 33.5908 132.5417)" fill="#fff" font-family="'Verdana'" font-size="7">12</text>
<text transform="matrix(-0.5446 -0.8387 0.8387 -0.5446 25.5063 122.0745)" fill="#fff" font-family="'Verdana'" font-size="7">8</text>
<text transform="matrix(-0.4067 -0.9135 0.9135 -0.4067 19.7744 114.3909)" fill="#fff" font-family="'Verdana'" font-size="7">19</text>
<text transform="matrix(-0.2419 -0.9703 0.9703 -0.2419 15.8628 104.0159)" fill="#fff" font-family="'Verdana'" font-size="7">31</text>
<text transform="matrix(-0.0872 -0.9962 0.9962 -0.0872 14.0029 92.946)" fill="#fff" font-family="'Verdana'" font-size="7">18</text>
<text transform="matrix(0.0872 -0.9962 0.9962 0.0872 13.3525 79.1257)" fill="#fff" font-family="'Verdana'" font-size="7">6</text>
<text transform="matrix(0.2419 -0.9703 0.9703 0.2419 14.1396 69.9651)" fill="#fff" font-family="'Verdana'" font-size="7">21</text>
<text transform="matrix(0.4067 -0.9135 0.9135 0.4067 17.7866 58.8904)" fill="#fff" font-family="'Verdana'" font-size="7">33</text>
<text transform="matrix(0.5446 -0.8387 0.8387 0.5446 22.0781 48.9431)" fill="#fff" font-family="'Verdana'" font-size="7">16</text>
<text transform="matrix(0.682 -0.7314 0.7314 0.682 30.2607 37.6848)" fill="#fff" font-family="'Verdana'" font-size="7">4</text>
<text transform="matrix(0.788 -0.6157 0.6157 0.788 36.9409 31.3318)" fill="#fff" font-family="'Verdana'" font-size="7">23</text>
<text transform="matrix(0.8829 -0.4695 0.4695 0.8829 45.3809 24.8455)" fill="#fff" font-family="'Verdana'" font-size="7">35</text>
<text transform="matrix(0.9455 -0.3256 0.3256 0.9455 55.2607 19.5925)" fill="#fff" font-family="'Verdana'" font-size="7">14</text>
<text transform="matrix(0.9877 -0.1564 0.1564 0.9877 68.606 15.9045)" fill="#fff" font-family="'Verdana'" font-size="7">2</text> */}
    </svg>
</div>