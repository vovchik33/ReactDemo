import {observable} from "mobx";

class MenuStore {
    @observable items=["item 1", "item 2"];
}

var store = window.store = new MenuStore

export default store