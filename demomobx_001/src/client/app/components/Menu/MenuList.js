import React, {Component} from 'react';
import {observer} from 'mobx-react';

@observer
export default class MenuList extends Component {
    render() {
        return (
            <div>
                    {this.props.store.items.map(function(item){
                        return <div key={item} >{item}</div>
                    })}
            </div>
        );
    }
}