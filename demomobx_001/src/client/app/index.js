import React, {Component} from 'react';
import {render} from 'react-dom';

import MenuList from './components/Menu/MenuList';
import store from './components/Menu/MenuStore';

class Main extends Component {
    render() {
        return (
            <div>
                <MenuList store={store}/>
            </div>
        );
    }
}

render(
    <div>Hello <Main/></div>,
    document.getElementById('appRoot')
)