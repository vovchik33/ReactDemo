import React, { Component } from 'react';
import usersData from '../../assets/data/fixtures';

import './styles.css';

function UserPic(props) {
    return <div className="user-pic">
        <img src={props.src} alt="UserPic"/>
    </div>;
}
function UserName(props) {
    return <div className="user-name">
        {props.nick}
    </div>;
}
class UsersList extends Component {
    currentIndex = 0;
    constructor(props) {
        super(props);
        this.state = {
            usersData: usersData
        }
        this.props.onUserClickHandler(usersData[this.currentIndex]);
    }

    clickUserHandler(index, event) {
        if (index!==this.currentIndex) {
            this.currentIndex = index;
            this.props.onUserClickHandler(this.state.usersData[this.currentIndex]);
        }
    }

    render() {
        return <div className="users-list">
            <h3>Users List:</h3>
            {
                this.state.usersData.map(function (user, index) {
                    const userClassName = "user-container"+(index===this.currentIndex?" active":"");
                    return <div key={index} onClick={this.clickUserHandler.bind(this, index)} className={userClassName}>
                        <UserPic src={user.img} />
                        <UserName nick={user.nickname} />
                    </div>
                }, this)
            } 
        </div>
    }
}

export default UsersList;