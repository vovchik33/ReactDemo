import React, {Component} from 'react';

import UsersList from '../UsersList';
import UserDetails from '../UserDetails';

import './styles.css';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    onUserClickHandler(data) {
        console.log("SELECT", data);
        this.setState({
            activeUser: data
        })
    }
    render() {
        return <div className="app">
            <UsersList onUserClickHandler={this.onUserClickHandler.bind(this)}/>
            <UserDetails userData={this.state.activeUser}/>
        </div>
    }
}

export default App;