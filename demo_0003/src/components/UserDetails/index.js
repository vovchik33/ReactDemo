import React, { Component } from 'react';

import './styles.css';

class UserDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: {nickname:'', img:''}
        }
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            userData: nextProps.userData
        });
    }
    render() {
        return <div className="user-details">
            <h3>User Details:</h3>
            {/* <h4>{(this.props.userData)?this.props.userData.nickname:''}</h4> */}
            <h4>{(this.state.userData)?this.state.userData.nickname:''}</h4>
            <img src={(this.state.userData)?this.state.userData.img:''} alt='UserPic'/>
        </div>
    }
}

export default UserDetails;