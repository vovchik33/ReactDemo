import React, {Component} from 'react'

class TimeCounter extends Component {
    constructor (props){
        super(props);
        this.state = {
            init: new Date(),
            duration: 60,
            current: 0,
            loop: true
        };
        this.state.init.setSeconds(this.state.init.getSeconds()+this.state.duration)
        this.timer = setInterval(tick.bind(this), 1000);
    }
    
    render() {
        return (
            <div>
                TimeCounter {this.state.current.toFixed(0)}
            </div>
        )
    }
}

function tick() {
    console.log("Timer is ticked!");
    var current = this.state.init-new Date();
    current = (current>0)?current/1000:0;
    if (current==0 && this.state.loop) {
        this.state.init.setSeconds(new Date().getSeconds()+this.state.duration);
    }
    this.setState({current:current});
}

export default TimeCounter