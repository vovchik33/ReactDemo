import React, { Component } from 'react'

class Header extends Component {
    constructor(props) {
        super(props);
        console.log('Header is created');
    }

    render() {
        return (
            <div className='alert alert-primary'>
                <h1>Extended Header</h1>
            </div>
        )
    }
}

export default Header;