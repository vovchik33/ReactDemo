import React from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import Header from './extended/Header';
import Footer from './extended/Footer';
import TimeCounter from './extended/TimeCounter';

function App() {
    return (
        <div className='App'>
            <Header />
            Some simple content 
            <TimeCounter />
            <Footer />
        </div>
    )
}

export default App