import React, { Component } from 'react'

class Article extends Component {
    constructor(props) {
        super(props);
        this.state = {
            likesCounter: 0
        }
        this.clickLikeHandler = clickLikeHandler.bind(this);
    }

    render() {
        const { article } = this.props
        const { active } = this.props

        return (
            <div className="card mx-auto">
                <div className="card-header">
                    <h2 style={{ color: active ? 'red' : 'black' }}>Article Component</h2>
                    <h3>creation date {(new Date(article.date)).toDateString()}</h3>
                </div>
                <div className="card-body">
                    <div>
                        {article.description}
                    </div>
                    <div>
                        {article.text}
                    </div>
                </div>
                <div className="card-subtitle text-muted ">
                    <div className="float-right">
                        <span>{this.state.likesCounter}</span>
                        <button className="btn btn-primary" onClick={this.clickLikeHandler}>Like</button>
                    </div>
                </div>
            </div>
        )
    }
}

function clickLikeHandler() {
    console.log("Amount " + this.likesCounter);
    this.setState({ likesCounter: this.state.likesCounter + 1 });

}
export default Article
