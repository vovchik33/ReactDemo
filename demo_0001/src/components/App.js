import React from 'react'
import ArticleList from './ArticleList'
import 'bootstrap/dist/css/bootstrap.css'


import articles from "../fixtures"

function App() {

    return (
        <div className="container">
            <h1 className="jumbotron">Application Component</h1>
            <ArticleList className="display-3" articles={articles}/>
        </div>
    )
}

export default App